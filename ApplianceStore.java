import java.util.Scanner;
public class ApplianceStore {
	public static void main(String[] args) {
		Lightbulb[] lightbulb = new Lightbulb[4];
		Scanner sc = new Scanner(System.in);
		String truth = "true";
		String isTrue = "";
		for(int i = 0; i < lightbulb.length; i++) {
			lightbulb[i] = new Lightbulb();
			System.out.println("please input wether the bulb is on or off for lightbulb # "+(i+1)+" [true/false].");
			isTrue = sc.nextLine();
			lightbulb[i].state = isTrue.equals(truth);
			System.out.println("please input the number of watts for lightbulb # "+(i+1)+" [integer].");
			lightbulb[i].watts = Integer.parseInt(sc.nextLine());
			System.out.println("please input the brand for lightbulb # "+(i+1)+" [String].");
			lightbulb[i].brand = sc.nextLine();
		}
		
		System.out.println("lightbulb 4 is on: "+lightbulb[3].state+". lightbulbe 4 has a capacity of "+lightbulb[3].watts+" watts. It's brand is "+lightbulb[3].brand+".");
		lightbulb[0].isOn(lightbulb[0].state);
		lightbulb[0].printBrand(lightbulb[0].brand);
	}
}